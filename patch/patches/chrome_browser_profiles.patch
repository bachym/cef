diff --git chrome/browser/profiles/profile.cc chrome/browser/profiles/profile.cc
index 8e8a7abdcf17..89c47d18d379 100644
--- chrome/browser/profiles/profile.cc
+++ chrome/browser/profiles/profile.cc
@@ -77,6 +77,7 @@ base::LazyInstance<std::set<content::BrowserContext*>>::Leaky
 
 namespace {
 
+const char kCEFOTRProfileIDPrefix[] = "CEF::BrowserContext";
 const char kDevToolsOTRProfileIDPrefix[] = "Devtools::BrowserContext";
 const char kMediaRouterOTRProfileIDPrefix[] = "MediaRouter::Presentation";
 
@@ -90,6 +91,8 @@ bool Profile::OTRProfileID::AllowsBrowserWindows() const {
   // DevTools::BrowserContext and MediaRouter::Presentation are an
   // exception to this ban.
   return *this == PrimaryID() ||
+         base::StartsWith(profile_id_, kCEFOTRProfileIDPrefix,
+                          base::CompareCase::SENSITIVE) ||
          base::StartsWith(profile_id_, kDevToolsOTRProfileIDPrefix,
                           base::CompareCase::SENSITIVE) ||
          base::StartsWith(profile_id_, kMediaRouterOTRProfileIDPrefix,
@@ -111,6 +114,11 @@ Profile::OTRProfileID Profile::OTRProfileID::CreateUnique(
                                          first_unused_index_++));
 }
 
+// static
+Profile::OTRProfileID Profile::OTRProfileID::CreateUniqueForCEF() {
+  return CreateUnique(kCEFOTRProfileIDPrefix);
+}
+
 // static
 Profile::OTRProfileID Profile::OTRProfileID::CreateUniqueForDevTools() {
   return CreateUnique(kDevToolsOTRProfileIDPrefix);
diff --git chrome/browser/profiles/profile.h chrome/browser/profiles/profile.h
index e77f4b15ce32..13569302c96b 100644
--- chrome/browser/profiles/profile.h
+++ chrome/browser/profiles/profile.h
@@ -116,6 +116,9 @@ class Profile : public content::BrowserContext {
     // Creates a unique OTR profile id with the given profile id prefix.
     static OTRProfileID CreateUnique(const std::string& profile_id_prefix);
 
+    // Creates a unique OTR profile id to be used for CEF browser contexts.
+    static OTRProfileID CreateUniqueForCEF();
+
     // Creates a unique OTR profile id to be used for DevTools browser contexts.
     static OTRProfileID CreateUniqueForDevTools();
 
diff --git chrome/browser/profiles/profile_manager.cc chrome/browser/profiles/profile_manager.cc
index 71730949d2bd..eedcde617f01 100644
--- chrome/browser/profiles/profile_manager.cc
+++ chrome/browser/profiles/profile_manager.cc
@@ -397,7 +397,7 @@ ProfileManager::ProfileManager(const base::FilePath& user_data_dir)
   registrar_.Add(this, chrome::NOTIFICATION_BROWSER_CLOSE_CANCELLED,
                  content::NotificationService::AllSources());
 
-  if (ProfileShortcutManager::IsFeatureEnabled() && !user_data_dir_.empty())
+  if (!user_data_dir_.empty() && ProfileShortcutManager::IsFeatureEnabled())
     profile_shortcut_manager_ = ProfileShortcutManager::Create(this);
 }
 
diff --git chrome/browser/profiles/profile_manager.h chrome/browser/profiles/profile_manager.h
index 743dccc6ddf4..204be4916f14 100644
--- chrome/browser/profiles/profile_manager.h
+++ chrome/browser/profiles/profile_manager.h
@@ -113,7 +113,7 @@ class ProfileManager : public content::NotificationObserver,
   // acceptable. Returns null if creation of the new profile fails.
   // TODO(bauerb): Migrate calls from other code to GetProfileByPath(), then
   // make this method private.
-  Profile* GetProfile(const base::FilePath& profile_dir);
+  virtual Profile* GetProfile(const base::FilePath& profile_dir);
 
   // Returns regular or off-the-record profile given its profile key.
   static Profile* GetProfileFromProfileKey(ProfileKey* profile_key);
@@ -146,7 +146,7 @@ class ProfileManager : public content::NotificationObserver,
 
   // Returns true if the profile pointer is known to point to an existing
   // profile.
-  bool IsValidProfile(const void* profile);
+  virtual bool IsValidProfile(const void* profile);
 
   // Returns the directory where the first created profile is stored,
   // relative to the user data directory currently in use.
@@ -155,7 +155,7 @@ class ProfileManager : public content::NotificationObserver,
   // Get the Profile last used (the Profile to which owns the most recently
   // focused window) with this Chrome build. If no signed profile has been
   // stored in Local State, hand back the Default profile.
-  Profile* GetLastUsedProfile(const base::FilePath& user_data_dir);
+  virtual Profile* GetLastUsedProfile(const base::FilePath& user_data_dir);
 
   // Get the path of the last used profile, or if that's undefined, the default
   // profile.
diff --git chrome/browser/profiles/renderer_updater.cc chrome/browser/profiles/renderer_updater.cc
index 04bbd816692b..1af61cacc5cb 100644
--- chrome/browser/profiles/renderer_updater.cc
+++ chrome/browser/profiles/renderer_updater.cc
@@ -8,6 +8,7 @@
 
 #include "base/bind.h"
 #include "build/chromeos_buildflags.h"
+#include "cef/libcef/features/runtime.h"
 #include "chrome/browser/content_settings/host_content_settings_map_factory.h"
 #include "chrome/browser/profiles/profile.h"
 #include "chrome/browser/signin/identity_manager_factory.h"
@@ -58,8 +59,12 @@ void GetGuestViewDefaultContentSettingRules(
 
 RendererUpdater::RendererUpdater(Profile* profile)
     : profile_(profile), identity_manager_observer_(this) {
+  if (cef::IsAlloyRuntimeEnabled()) {
+    identity_manager_ = nullptr;
+  } else {
   identity_manager_ = IdentityManagerFactory::GetForProfile(profile);
   identity_manager_observer_.Add(identity_manager_);
+  }
 #if BUILDFLAG(IS_CHROMEOS_ASH)
   oauth2_login_manager_ =
       chromeos::OAuth2LoginManagerFactory::GetForProfile(profile_);
